import math


def is_string_code_word(checked_string: str) -> bool:
    """Checks if a string that is checked is a code word

    Arguments:
        checked_string {str} -- string to be checked

    Returns:
        boolean -- is checked string a valid code word
    """
    for checked_character in checked_string:
        if checked_character != "0" and checked_character != "1":
            return False

    return True


def get_code_word_fixed_length(code_word: str) -> int:
    """Gets length of code words in block code K -> n

    Arguments:
        code_word {str} -- code word that is analyzed

    Returns:
        int -- length of code words
    """
    return len(code_word)


def get_number_of_code_words(code_word_list: list) -> int:
    """Gets number of code words in block code K -> M

    Arguments:
        code_word_list {list} -- code word list that is analyzed

    Returns:
        int -- number of code words
    """
    return len(code_word_list)


def get_number_of_informational_bits(number_of_code_words: int) -> int:
    """Gets number of informational bits in code words of code K -> k

    Arguments:
        number_of_code_words {int} -- M (number of code words in code K)

    Returns:
        int -- number of informational bits in code words
    """
    return int(math.log2(number_of_code_words))


def determine_code_minimal_distance(code_word_fixed_length: int, code_word_list: list) -> int:
    """Gets code minimal distance -> d(K)

    Arguments:
        code_word_fixed_length {int} -- n (code words length)
        code_word_list {list} -- list of code words in code K

    Returns:
        int -- code minimal distance
    """
    ## initially set minimal code distance to be of codew words length and then set to smaller number if smaller distance is found
    minimal_code_distance = code_word_fixed_length

    for i in range(len(code_word_list)):
        for j in range(i + 1, len(code_word_list)):
            code_words_distance = get_code_word_distance(code_word_list[i], code_word_list[j])
            if code_words_distance < minimal_code_distance:
                minimal_code_distance = code_words_distance

    return minimal_code_distance


def get_maximum_number_of_errors_discovered(code_word_list_minimal_distance: int) -> int:
    """Gets maximum number of errors that can be detected.

    Arguments:
        code_word_list_minimal_distance {int} -- d(K) of code K

    Returns:
        int -- maximum number of errors code K can detect
    """
    return code_word_list_minimal_distance - 1


def get_maximum_number_of_errors_corrected(code_word_list_minimal_distance: int) -> int:
    """Gets maximum number of errors that can be corrected.

    Arguments:
        code_word_list_minimal_distance {int} -- d(K) of code K

    Returns:
        int -- maximum number of errors code K can correct
    """
    return int((code_word_list_minimal_distance - 1) / 2.0)


def get_code_perfection_coefficient(code_word_fixed_length: int, maximum_number_of_errors_corrected: int) -> float:
    """Gets code perfection coefficient (to be compared to M (number of words in code K) afterwards).

       Arguments:
           code_word_fixed_length {int} -- n (length of block code words in code K)
           maximum_number_of_errors_corrected {int}  -- t (maximum number of errors that can be corrected)

       Returns:
           float -- perfection coefficient of code K
       """
    sum_of_binoms = get_sum_of_binoms(code_word_fixed_length, maximum_number_of_errors_corrected)

    return math.pow(2, code_word_fixed_length) / sum_of_binoms


def is_code_perfect(number_of_code_words: int, code_perfection_coefficent: float) -> bool:
    """Determines if the code K is perfected by testing its parameters against code perfection condition.

       Arguments:
           number_of_code_words {int} -- M (number of code words in code K)
           code_perfection_coefficent {float} -- perfection coefficient of code K

       Returns:
           bool -- true if code K is perfect (condition for code K to be prefect is fulfilled)
       """
    return number_of_code_words == code_perfection_coefficent


def get_code_word_distance(code_word1: str, code_word2: str) -> int:
    """Gets distance(number of differences in bits) between two code words.

    Arguments:
        code_word1 {str} -- code word 1
        code_word2 {str} -- code word 2

    Returns:
        int -- number of differences in two code words
    """
    code_distance = 0
    for i in range(len(code_word1)):
        if code_word1[i] != code_word2[i]:
            code_distance += 1

    return code_distance


def get_binom(n: int, r: int) -> int:
    """Calculates binomial coefficient, nCr
        n! / (r! * (n - r)!)

    Arguments:
        n {int}
        r {int}

    Returns:
        int -- binomial coefficeient
    """
    try:
        binom = math.factorial(n) // math.factorial(r) // math.factorial(n - r)
    except ValueError:
        binom = 0
    return binom


def get_sum_of_binoms(n: int, t: int) -> int:
    """Gets sum of binomial coefficients:
    sum = C(n, 0) + C(n, 1) + ... + C(n, t)

    Where C(x, y) is binomial coefficient
    Arguments:
        n {int}
        t {int}

    Returns:
        int -- sum of binomial coefficeients
    """
    sum_of_binoms = 0
    for i in range(t + 1):
        sum_of_binoms += get_binom(n, i)

    return sum_of_binoms


def is_code_word_zero_in_list(code_word_list: list) -> bool:
    """Checks if there is code word that consits of only zeros
    in given code word list

    Arguments:
        code_word_list {list} -- given code word list

    Returns:
        bool -- if code word zero is in given code word list
    """
    for code_word in code_word_list:
        if "1" not in code_word:
            return True

    return False


def get_sum_of_two_code_words(code_word1: str, code_word2: str) -> str:
    """Calculates sum of two code words.

    Arguments:
        code_word1 {str} -- String representation of code word.
        code_word2 {str} -- String representation of code word.

    Returns:
        str -- String represenation of summed code word.
    """
    code_word1_num = int(code_word1, base=2)
    code_word2_num = int(code_word2, base=2)

    # string return by bin function returns binary with 0b at beginning
    # this removes those first signs and then fills zeroes on the left to specifed length(the length of code word)
    code_word_sum = bin(code_word1_num ^ code_word2_num)[2:].zfill(len(code_word1))

    return code_word_sum


def check_code_words_sum(code_word_list: list) -> bool:
    """Checks if all possible sums in code word list
    give another code word that is inside code word list.

    Arguments:
        code_word_list {list} -- given code word list

    Returns:
        bool -- do all sums return a code word that is inside code word list
    """
    for i in range(len(code_word_list)):
        for j in range(i + 1, len(code_word_list)):
            sum_of_code_words = get_sum_of_two_code_words(code_word_list[i], code_word_list[j])
            if sum_of_code_words not in code_word_list:
                return False

    return True


def is_code_linear(code_word_list: list) -> bool:
    """Check if given code by code word list is
    linear.
    This checks two rules:
    1. Sum of each code word in given parameter list must produce a code word that is in the list
    2. There must be a code word with zeros in it.
    Arguments:
        code_word_list {list} -- Code word list to be checked.

    Returns:
        bool -- Is code linear
    """
    return is_code_word_zero_in_list(code_word_list) and check_code_words_sum(code_word_list)


def print_error_no_code_words_entered():
    """Prints error if no code words are entered.
    """
    print("Nijedna kodna riječ nije upisana. Kraj rada.")


def print_error_incorrect_number_of_code_words():
    """Prints error if number of code words in code K is not power of 2.
    """
    print("Broj kodnih riječi nije potencija broja 2. Kraj rada.")


def printCodeWords(code_word_list: list):
    """ Prints code words in code K.
    """
    for code_word in code_word_list:
        print(code_word)


def main():
    code_word = input()
    code_word_fixed_length = None
    # Empty string - exit instantly
    if len(code_word) == 0:
        print_error_no_code_words_entered()
        return

    code_word_list = []

    while code_word:
        if not is_string_code_word(code_word):
            print("Upisana vrijednost ne moze biti kodna rijec jer sadrzi znakove razlicite od 0 i 1.")
            print("Upisite kodnu rijec ponovo:")
            code_word = input()
            continue

        if code_word_fixed_length is None:
            code_word_fixed_length = get_code_word_fixed_length(code_word)

        if len(code_word) != code_word_fixed_length:
            print("Upisana vrijednost ne moze biti kodna rijec jer ova rijec ima duljinu razlicitu od prethodnih.")
            print("Upisite kodnu rijec ponovo:")
            code_word = input()
            continue

        if code_word in code_word_list:
            print("Upisana vrijednost vec postoji kao kodna rijec.")
            print("Upisite kodnu rijec ponovo:")
            code_word = input()
            continue

        code_word_list.append(code_word)
        code_word = input()

    number_of_code_words = get_number_of_code_words(code_word_list)
    if number_of_code_words == 0:
        print_error_no_code_words_entered()
        return

    number_of_informational_bits = get_number_of_informational_bits(number_of_code_words)
    if (number_of_code_words != 2**number_of_informational_bits):
        print_error_incorrect_number_of_code_words()
        return

    print("---------------------")
    print("Kod K:")
    printCodeWords(code_word_list)
    print("---------------------")

    print("Analiza koda K:")
    print("Broj kodnih rijeci u kodu:")
    print(f"M = {number_of_code_words}")

    print("Duljina kodne rijeci:")
    print(f"n = {code_word_fixed_length}")

    print("Broj informacijskih bitova:")
    print(f"k = {number_of_informational_bits}")

    code_word_list_minimal_distance = determine_code_minimal_distance(code_word_fixed_length, code_word_list)
    print("Minimalna udaljenost koda:")
    print(f"d(K) = {code_word_list_minimal_distance}")

    maximum_number_of_errors_discovered = get_maximum_number_of_errors_discovered(code_word_list_minimal_distance)
    print("Broj gresaka koje kod moze otkriti:")
    print(f"s = {maximum_number_of_errors_discovered}")

    maximum_number_of_errors_corrected = get_maximum_number_of_errors_corrected(code_word_list_minimal_distance)
    print(f"Broj gresaka koje kod moze ispraviti:")
    print(f"t = {maximum_number_of_errors_corrected}")

    print("Za svaki kod vrijedi:")
    print("M <= 2^n / (C(n, 0) + C(n, 1) + ... +C(n, t))")

    print("Kod je perfektan ako vrijedi:")
    print("M = 2^n / (C(n, 0) + C(n, 1) + ... +C(n, t))")

    code_perfection_coefficent = get_code_perfection_coefficient(code_word_fixed_length,
                                                                 maximum_number_of_errors_corrected)

    if is_code_perfect(number_of_code_words, code_perfection_coefficent):
        print(f"Kod je perfektan jer: {number_of_code_words} = {code_perfection_coefficent}")
    else:
        print(f"Kod nije perfektan jer: {number_of_code_words} < {code_perfection_coefficent}")

    if is_code_linear(code_word_list):
        print("Kod je linearan")
    else:
        print("Kod nije linearan")


if __name__ == '__main__':
    main()
