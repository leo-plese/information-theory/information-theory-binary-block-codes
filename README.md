# Information Theory Binary Block Codes

Analysis of input binary block code (K) which determines: number of codewords in the code (M), codeword length (n), number of information bits (k), minimal code distance of the code (d(K)), number of errors which can be detected, number of errors which can be corrected, if codes is perfect and if code is linear.

Implemented in Python using math library.

My project in Information Theory, FER, Zagreb.

Created: 2020